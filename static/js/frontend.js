
isDebug=true;
function log(k)
{
    if(isDebug)
        console.log(k)
}
var is=0;
$(document).ready(function(){

    // выравнивание лого в шапке по вертикали
/*   $.fn.vAlign = function() {
    return this.each(function(i){
        var ah = $(this).height();
        var ph = $(this).parent().height();      
        var mh = Math.ceil((ph-ah) / 2); //var mh = (ph - ah) / 2;
        $(this).css('padding-top', mh);
      });
    };

    $(".main-logo").vAlign();
    window.onload = function() {
        $(".main-logo").vAlign();
    };
*/
    $(".spinner").spinner();

    $(".andmore").click(function () {
        $(this).parents().find(".notify-hidden").toggle(200);
    });

    $(".popover.hide.fade").modal({
        "show":false,
        "backdrop":false
    });

    $('.more-info').not('[class~="top-menu__more-info"]').popover({ title:"Справка." });

    $('form').submit(function()
    {
        // Блокируем кнопки при отправке формы
        $('input[type=submit]', this).attr('disabled', 'disabled');
    });



//    $('.resize_auto').autoResize({
//        // On resize:
//        onResize : function() {
//            $(this).css({opacity:0.8});
//        },
//        // After resize:
//        animateCallback : function() {
//            $(this).css({opacity:1});
//        },
//        // Quite slow animation:
//        animateDuration : 300,
//        // More extra space:
//        extraSpace : 40
//    });


    //форма "Задать вопрос" в левой колонке
    $(".ask-question-bar.hidden_form h2").click(function(){
    	if ( $(this).parents(".ask-question-bar").hasClass('hidden_form') ) {
	       $(this).parents(".ask-question-bar").removeClass("hidden_form");
	       $(this).parent().next().show("fast");
    	} else {
	       $(this).parents(".ask-question-bar").addClass("hidden_form");
	       $(this).parent().next().hide("fast");
    	}
    	return false;
    });

    // карусель в блоке поиска в шапке
    $('.resultat').button()

    var clickSpeed = 500; // the speed of animation when clicking a nav button
    var autoSpeed = 500; // when autoscrolling to right
    var repeatSpeed = 6000; // time when to repeat the autoScroll
    var moveflag = 0;
    // Non editable vars
    var scroll;
    var timerID = null;

    // Set the left css style - so that we have the first element hidden on the left
    // using setTimeout - for Chrome
    setTimeout(function(){
        var w = $('#carousel_ul li:first').outerWidth() + 10;
        $('#carousel_ul').css({'left' : w * -1});
    },1);

    //when user clicks the image for sliding right
    $('#right_scroll').click(scrollRigth);

    //when user clicks the image for sliding left
    $('#left_scroll').click(ScrollLeft);
    
    function ScrollLeft(){
        clearInterval (timerID); // clear the interval - disable autoscrolling
        disableNavButtons(); // disable Nav Buttons when scrolling is happening

        var item_width = $('#carousel_ul li').outerWidth() + 10;

        // same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width)
        var left_indent = parseInt($('#carousel_ul').css('left')) + item_width;
        var $last = $('#carousel_ul li:last');

        $('#carousel_ul:not(:animated)').animate({'left' : left_indent},clickSpeed,function(){
            var width = $last.outerWidth() + 10;
            $('#carousel_ul li:first').before( $last);
            $last.click(FillSearchInput);
            $('#carousel_ul').css({'left' : width * -1});

            enableNavButtons(); // Enable Nav Buttons back
            timerID = null;
            startAutoScroll(); // reset timer
        });
    }
    
    function scrollRigth(){
        clearInterval (timerID); // clear the interval - disable autoscrolling
        disableNavButtons(); // disable Nav Buttons when scrolling is happening

        var item_width = $('#carousel_ul li').outerWidth() + 10;

        // same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width)
        var left_indent = parseInt($('#carousel_ul').css('left')) - item_width;
        var $first = $('#carousel_ul li:first');

        $('#carousel_ul:not(:animated)').animate({'left' : left_indent}, clickSpeed, function(){
            var width = $first.outerWidth() + 10;
            $('#carousel_ul li:last').after( $first);
            $('#carousel_ul').css({'left' : left_indent+width });
            

            enableNavButtons(); // Enable Nav Buttons back
            timerID = null;
            startAutoScroll(); // reset timer
        });
    }

    function enableNavButtons()
    {
        $('#right_scroll').click(scrollRigth);
        $('#left_scroll').click(ScrollLeft);
        $('#right_scroll img').removeAttr('disabled');
        $('#left_scroll img').removeAttr('disabled');
    }

    function disableNavButtons()
    {
        $('#right_scroll').unbind("click");
        $('#left_scroll').unbind("click");
        $('#right_scroll img').attr('disabled', true);
        $('#left_scroll img').attr('disabled', true);
    }

    function startAutoScroll()
    {
        if (timerID === null) { // to avoid multiple registration
            timerID = setInterval (function(){scrollRigth(autoSpeed);}, repeatSpeed);
        }
    }
    startAutoScroll();


    // Расрывающееся дерево на стр search4
    $(".tree_branch a").bind('click',function(){

    $(this).parent().find(".wood_box").toggle();

    /*$(this).parent().children(".active").removeClass("active");
    $(this).parent().children(".active").find(".active").removeClass("active");
    $(this).addClass("active").slideDown();*/
    return false;
    });
       
    $(".tree_branch2 a").bind('click',function(){
        $(this).parent().find(".wood_box2").toggle();
    return false;
    }); 
    

    
    // slider на стр search_results
    $( "#slider" ).slider();

//всплывающие окна (Расширенный поиск, стр search_results)
    hideEvent = null;
    $('.popup-onhover-show').hover(function(){
        var offset = $(this).offset();
        var x = offset.left;
        var y = offset.top;
        var withObject = $(this).width();
        var popupObject = '#' + $(this).attr('rel');
        var withpopupObject = $(popupObject).width();
        var margenObject = -(withpopupObject)/2;
        //console.log(withpopupObject)
        $(popupObject ).show(100).css('marginLeft',margenObject).css('top',y ).css('left',x+(withObject/2));
        clearTimeout(hideEvent);
    }, function(){
        hideEvent = setTimeout("$('.popup-onhover').hide();", 300);
    });

    $('.popup-onhover').hover(function(){
        clearTimeout(hideEvent);
    }, function(){
        hideEvent = setTimeout("$('.popup-onhover').hide();", 300);
    })

    //оформление селектора
    $('.selectBlock').sSelect();

    //личный кабинет
    $(".informationMini").click(function(){
        $(this).toggleClass("active");
        $(this).next(".informationAll").toggle();
        //$(this).next(".informationAll").find("table").toggle();//hak ie7
    });

    $('.map').click(function(){//всплывающая карта схемы проезда
        $('#myMap').click();
    });

//    $(".update-captcha").click(function(){
//
//        log("updating")
//        $(".captcha").attr("src", "/captcha?"+Math.random());
//        return false;
//    });

    $('[rel=tooltip],[data-tooltip],').tooltip();
    
    /**
     * Рейтинги: звездочки наши. Тут анимация и обработка клика. Хэндлер тоже писать сюда
     */
	$('.rate:not(.static)').hover(function(){
		$('.rate li').hover(function(){
			if ( $(this).parent().attr('rel') !== undefined ) {
				$(this).parent().find('li').removeClass('a');
				$(this).parent().find('li:lt(' + $(this).index() + ')').addClass('tmp_active');
				$(this).addClass('tmp_active');
			}
		}, function(){
			$(this).removeClass('tmp_active');
		});
	}, function(){
		var index = parseInt($(this).attr('rel'));
		
		$(this).find('li').removeClass('tmp_active');
		$(this).find('li:lt(' + index + ')').addClass('a');
	});
	$('.rate li').click(function(){
		alert('А тут надо писать аякс для обработка клика');
	});
    
    // активы для кнопок кторе вызвают модалное окно
    $("#noticeModal").on('hidden', function () {
        $(".top-ponel__notice > a").removeClass("active");
    });

    $('.modal').on('show', function(){
        $(this).css('top', $(window).scrollTop() + 100);
    });

});

//function FillSearchInput()
//{
//    //  console.log($(this));
//    $('#search-input').val($.trim($(this).text()));
//    $('#search-input').focus();
//
//    return false;
//}