$(document).ready(function() // После загрузки страницы
{
	$(".removeParam").click(RemoveStr);
	$(".addRow").click(AddStr);

    if($("#addressDelivery-0:selected").length)
    {
        $("#new_address").hide();
    }
});

function RemoveStr()
{
    $(this).parent().parent().fadeOut("slow", function(){
        $(this).remove();
    });
    return false;
}

function AddStr()
{
    var tr = $("<tr>");
    tr.append($("<td>"));
    var input = $("<input>").attr("name", "params["+nextI+"][name]").attr("id", "params["+nextI+"][name]");
    tr.append($("<td>").append(input));
    input = $("<input>").attr("name", "params["+nextI+"][alias]").attr("id", "params["+nextI+"][alias]");
    tr.append($("<td>").append(input));
    input = $("<input>").attr("type", "checkbox").attr("name", "params["+nextI+"][isUser]").attr("id", "params["+nextI+"][isUser]");
    tr.append($("<td>").css({'text-align':"center"}).append(input));
    input = $("<input>").attr("type", "checkbox").attr("name", "params["+nextI+"][isRequired]").attr("id", "params["+nextI+"][isRequired]");
    tr.append($("<td>").css({'text-align':"center"}).append(input));
    input = $("<input>").attr("type", "checkbox").attr("name", "params["+nextI+"][isRuntime]").attr("id", "params["+nextI+"][isRuntime]");
    tr.append($("<td>").css({'text-align':"center"}).append(input));
    var link = $("<a>").attr("href", "#").addClass("removeParam").text("Удалить").click(RemoveStr);
    tr.append($("<td>").append(link));
    tr.append($("<input>").attr("name", "params["+nextI+"][id]").attr("id", "params["+nextI+"][id]").attr("type","hidden"));
    $("#params_table").append(tr);

    nextI++;
    return false;
    /**
     * <tr>
                <td></td>
                <td><input type="text" name="params[1][name]" id="params[1][name]" value=""/></td>
                <td><input type="text" name="params[1][alias]" id="params[1][alias]" value=""/></td>
                <td><a href="#" class="removeParam" >Удалить</a></td>
                <input type="hidden" name="params[1][id]" id="params[1][id]" value=""/>            </tr>
     */
}