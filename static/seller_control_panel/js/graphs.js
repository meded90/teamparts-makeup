$(document).ready(function(){

/*первый график - соотношение расходов и доходов*/
    $(function () {

        var data = [[55000, 20000], [20000, 30000], [60000, 33000], [39000, 50000], [63000, 22000], [37000, 46000] ];
        var captions = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь"];

        var paper = Raphael("balance");

        var fin = function () {
            this.flag = paper.popup(this.bar.x, this.bar.y, this.bar.text || "").insertBefore(this);
        };

        var fout = function () {
            this.flag.animate({ opacity: 0 }, 300, function () { this.remove(); });
        };

        //TEMP:
        //paper.rect(10, 10, 500, 320);

        var b = paper.ourbarchart(
            10, 35, 532, 358, data, //x,y,width,height,data
            {
                type: "soft",
                gutter: "70%",
                colors: ["#f8dbcd", "#f0f8cd"],
                borderColors: ["#e4b39c", "#b8db8f"],
                xOffset: "25%",
                yOffset: "30%",
                captionsWidth: 80,
                captions: captions,
                captionsFontFamily: "Arial",
                captionsFontSize: 13,
                profitFontFamily: "Arial",
                profitFontSize: 12,
                profitTextColor: "White",
                profitBackColor: "#26f"
            })
            .hover(fin, fout);

    });



/*второй график - доходы*/

    //данные для графика, формат [x,y]
    var d1 = [ [1, 12], [2, 145], [3, 46], [4, 79],[5, 24],[6, 120],[7, 55], [8, 32], [9, 46], [10, 79], [11, 24], [12, 120] ];

    //отметки на горизонтальной оси - названия
    var ticksX= [ [1, "Янв"], [2, "Февр"], [3, "Март"], [4, "Апр"],[5, "Май"],[6, "Июнь"],[7, "Июль"], [8, "Авг"],[9, "Сент"], [10, "Окт"], [11, "Нояб"], [12, "Дек"] ];

    //точки по вертикальной оси
    var ticksY = [ [-40, "-40 k"],[-20, "-20 k"], [0, "0 k"], [20, "20 k"], [40, "40 k"], [60, "60 k"],[80, "80 k"], [100, "100 k"], [120, "120 k"], [140, "140 k"], [160, "160 k"], [180, "180 k"], [200, "200 k"] ];

    $.plot($("#revenue"), [
        {
            data: d1,
            curvedLines: {
                apply: true
            },
            hoverable: false,
            label: "Ваша прибыль"
        },
        {
            data: d1,

            lines: {
                show: false
            },

            points : {
                show : true,
                radius: 5,
                fill: true,
                fillColor: '#42ace8'
            },
            hoverable: true
        }],

        {
            series : {
                curvedLines : {
                    active : true
                },
                lines: {
                    show: true,
                    lineWidth: 6
                },
                points : {
                    show : false
                },
                shadowSize: 4
            },
            xaxis : {
                min:1,
                max: d1.length,
                ticks: ticksX
            },
            yaxis : {
                min : -60,
                max : 200,
                labelWidth: 30,
                ticks: ticksY
            },
            colors: [ '#42ace8', '#42ace8'],
            grid: {
                hoverable: true,
                tickColor: "transparent",
                color: '#000',
                backgroundColor: '#f2f8fc',
                borderColor: '#8ebfdb',
                labelMargin: 5,
                borderWidth: 1,
                markings: function (axes) {
                    var markings = [];    //голубые вертикальные линии на полотне графика, привязанные к точкам горизонтальной оси
                    for (var x = Math.floor(axes.xaxis.min + 1); x < axes.xaxis.max; x ++) {
                        markings.push({ xaxis: { from: x, to: x }, color: "#cbdfeb" });
                    }
                    return markings;
                }
            },
            tooltip: true,
            tooltipOpts: {
                content: '%y.3 руб',
                shifts: {
                    x: -50,
                    y: -50
                },
                defaultTheme: false
            } ,
            legend: {
                position: "nw",
                labelBoxBorderColor: "#fff",
                margin: [15, 10]
            }
        }
    );

    //adjusting width of x-axis labels
    var allWidth = $('#revenue').width() + 9;
    var eachWidth = allWidth / d1.length;
    $('div.xAxis div.tickLabel').each(function(i) {
        $(this).css("width", eachWidth).css("left", 11 + i * eachWidth).css("text-align", "center");
    });




/*третий график - расходы*/

    var data = [
                ['Заказано деталей на сумму', 19],['Заказов в работе', 21], ['Возвраты клиентам', 18],['На счет Teampats зачислено', 13],
               ['Абонентская плата за сервис', 16], ['Еще какое-то поле', 16], ['и Еще какое-то поле', 12], 
            ];
    var plot1 = jQuery.jqplot ('expenses', [data],
        {
            seriesDefaults: {
                // Make this a pie chart.
                renderer: jQuery.jqplot.PieRenderer,
                rendererOptions: {
                    diameter: 580,
                    showDataLabels: true,
                    sliceMargin: 4,
                    padding: 0,
                    dataLabelNudge: 30, //расстояние от центра до подписей на кусках диаграммы
                    dataLabelThreshold: 1 //минимальное значение, для которого отображается подпись
                },
                seriesColors: [ "#9ed2f2", "#c8a6e5", "#de5ab4", "#f09266", "#e8c573", "#c7de88", "#61bb69", "#c68f47", "#25cec8", "#2b80ce"]
            },
            legend: {
                show:true,
                location: 'sw',
                xoffset: 0,
                yoffset: 0
            },

            title: 'Статьи расходов',
            grid: {
                background: ($.browser.msie && parseInt($.browser.version) < 9) ? '#fff' : 'transparent', /**а для ие8 белым фон должен быть**/
                borderWidth: 0,
                shadow: false
            }

        }
    );

    //enables tooltip
    $("#expenses").bind('jqplotDataHighlight', function(ev, seriesIndex, pointIndex, data) {
        var $this = $(this);

        $this.attr('title', data[0] + ": " + data[1]);
    });

    $("#expenses").bind('jqplotDataUnhighlight', function(ev, seriesIndex, pointIndex, data) {
        var $this = $(this);

        $this.attr('title',"");
    });

});