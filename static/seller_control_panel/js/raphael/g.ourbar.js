/*!
 * g.Raphael 0.51 - Charting library, based on Raphaël
 *
 * Copyright (c) 2009-2012 Dmitry Baranovskiy (http://g.raphaeljs.com)
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) license.
 */
(function () {
    var mmin = Math.min,
        mmax = Math.max;

    function finger(x, y, width, height, ending, isPath, paper) {
        var path,
            ends = { round: 'round', sharp: 'sharp', sharpReverse: 'sharpReverse', soft: 'soft', square: 'square' };

        if (!width) {
            return isPath ? "" : paper.path();
        }

        ending = ends[ending] || "square";
        height = Math.round(height);
        width = Math.round(width);
        x = Math.round(x);
        y = Math.round(y);

        switch (ending) {
            case "round":
                var r = ~ ~(height / 2);

                if (width < r) {
                    r = width;
                    path = [
						"M", x + .5, y + .5 - ~ ~(height / 2),
						"l", 0, 0,
						"a", r, ~ ~(height / 2), 0, 0, 1, 0, height,
						"l", 0, 0,
						"z"
					];
                } else {
                    path = [
						"M", x + .5, y + .5 - r,
						"l", width - r, 0,
						"a", r, r, 0, 1, 1, 0, height,
						"l", r - width, 0,
						"z"
					];
                }
                break;
            case "sharp":
                var half = ~ ~(height / 2);

                path = [
					"M", x, y + half,
					"l", 0, -height, mmax(width - half, 0), 0, mmin(half, width), half, -mmin(half, width), half + (half * 2 < height),
					"z"
				];
                break;
			case "sharpReverse":
                var half = ~ ~(height / 2);

                path = [
					"M", x + width, y + half,
					"l", 0, -height, -mmax(width - half, 0), 0, -mmin(half, width), half, mmin(half, width), half + (half * 2 < height),
					"z"
				];
                break;
            case "square":
                path = [
					"M", x, y + ~ ~(height / 2),
					"l", 0, -height, width, 0, 0, height,
					"z"
				];
                break;
            case "soft":
                r = mmin(width, Math.round(height / 10));
                path = [
					"M", x + .5, y + .5 - ~ ~(height / 2),
					"l", width - r, 0,
					"a", r, r, 0, 0, 1, r, r,
					"l", 0, height - r * 2,
					"a", r, r, 0, 0, 1, -r, r,
					"l", r - width, 0,
					"z"
				];
        }

        if (isPath) {
            return path.join(",");
        } else {
            return paper.path(path);
        }
    }

    //inheritance
    var F = function () { };
    F.prototype = Raphael.g;
    HBarchart.prototype = new F; //prototype reused by hbarchart

    function HBarchart(paper, x, y, width, height, values, opts) {
        opts = opts || {};

        var chartinst = this,
            type = opts.type || "square",
            gutter = parseFloat(opts.gutter || "20%"),
            chart = paper.set(),
            bars = paper.set(),
            covers = paper.set(),
            covers2 = paper.set(),
            total = 0,
            stacktotal = [],
            multi = 0,
            colors = opts.colors || chartinst.colors,
            len = values.length;
			
		var captionsWidth = opts.captionsWidth || 80;
		var captionsFontFamily = opts.captionsFontFamily || "Arial";
		var captionsFontSize = opts.captionsFontSize || 14;
		
		var profitFontFamily = opts.profitFontFamily || "Arial";
		var profitFontSize = opts.profitFontSize || 14;
		var profitTextColor = opts.profitTextColor || "White";
		var profitBackColor = opts.profitBackColor || "#26f";
		
		var profitWidth = 140;

        for (var i = 0; i < values.length; i++) {
            total = mmax(total, values[i][0]);
            total = mmax(total, values[i][1]);
        }

        var barheight = Math.floor(height / (len * (100 + gutter) + gutter) * 100),
            bargutter = Math.floor(barheight * gutter / 100),
            Y = y + bargutter,
            X = (width - captionsWidth - profitWidth - 1) / total;
			
		var xOffsets = [captionsWidth, captionsWidth + Math.floor(barheight * parseFloat(opts.xOffset || "20%") / 100)];
		var yOffsets = [0, Math.floor(barheight * parseFloat(opts.yOffset || "20%") / 100)];

        for (var i = 0; i < len; i++) {
            for (var j = 0; j < 2; j++) {
                var val = values[i][j],
                    bar = finger(x + xOffsets[j], Y + barheight / 2 + yOffsets[j], Math.round(val * X), barheight - 1, type, null, paper).attr({ stroke: opts.borderColors[j] || "none", fill: colors[j] });
				
				bars.push(bar);

                bar.x = x + Math.round(val * X) + xOffsets[j];
                bar.y = Y + barheight / 2 + yOffsets[j] - 5;
                bar.w = Math.round(val * X);
                bar.h = barheight + yOffsets[j];
                bar.value = +val;
				bar.text = ((j == 0) ? "Расходы" : "Доходы") + ":\n" + val + " руб.";
            }
			
			if (opts.captions[i]) {
				paper.text(x + 5, Y + barheight / 2, opts.captions[i]).attr(
				  { "font-family": captionsFontFamily,
					"font-size": captionsFontSize,
					"text-anchor":"start",
                    "fill": "#a3a3a3"
				  });
			}
			
			var profitPadding = 10;
			var profitX = x + profitPadding;
			if (values[i][0] > values[i][1]) {
				profitX += Math.round(values[i][0] * X) + xOffsets[0];
			}
			else {
				profitX += Math.round(values[i][1] * X) + xOffsets[1];
			}
			
			finger(profitX, Y + barheight / 2 + yOffsets[1] / 2, profitWidth - 2 * profitPadding, barheight, "sharpReverse", null, paper)
				.attr({ stroke: "none", fill: profitBackColor });
			
			var profitText = "";
			if (values[i][0] > values[i][1]) {
				profitText += "- " +(values[i][0] - values[i][1]);
			}
			else {
				profitText += "+ " + (values[i][1] - values[i][0]);
			}
			profitText += " руб.";
			paper.text(profitX + 20, Y + barheight / 2 + yOffsets[1] / 2, profitText).attr(
				  { "font-family": profitFontFamily,
					"font-size": profitFontSize,
					"fill": profitTextColor,
					"text-anchor":"start",
                    "font-weight":"bold"
				  });
			
            Y += barheight + bargutter;
        }

        covers2.toFront();
        Y = y + bargutter;
		
		for (var i = 0; i < len; i++) {
			for (var j = 0; j < 2; j++) {
				var bar = bars[2 * i + j];
				var cover = paper.rect(x + xOffsets[j], Y + yOffsets[j], bar.w, barheight).attr(chartinst.shim);

				covers.push(cover);
				cover.bar = bar;
				cover.value = cover.bar.value;
			}

			Y += barheight + bargutter;
		}

        chart.label = function (labels, isRight) {
            labels = labels || [];
            this.labels = paper.set();

            for (var i = 0; i < len; i++) {
                for (var j = 0; j < multi; j++) {
                    var label = paper.labelise(multi ? labels[j] && labels[j][i] : labels[i], multi ? values[j][i] : values[i], total),
                        X = isRight ? bars[i * (multi || 1) + j].x - barheight / 2 + 3 : x + 5,
                        A = isRight ? "end" : "start",
                        L;

                    this.labels.push(L = paper.text(X, bars[i * (multi || 1) + j].y, label).attr(txtattr).attr({ "text-anchor": A }).insertBefore(covers[0]));

                    if (L.getBBox().x < x + 5) {
                        L.attr({ x: x + 5, "text-anchor": "start" });
                    } else {
                        bars[i * (multi || 1) + j].label = L;
                    }
                }
            }

            return this;
        };

        chart.hover = function (fin, fout) {
			covers2.hide();
            covers.show();
            fout = fout || function () { };
            covers.mouseover(fin).mouseout(fout);
            return this;
        };

        chart.hoverColumn = function (fin, fout) {
            covers.hide();
            covers2.show();
            fout = fout || function () { };
            covers2.mouseover(fin).mouseout(fout);
            return this;
        };

        chart.each = function (f) {
            if (!Raphael.is(f, "function")) {
                return this;
            }
            for (var i = covers.length; i--; ) {
                f.call(covers[i]);
            }
            return this;
        };

        chart.eachColumn = function (f) {
            if (!Raphael.is(f, "function")) {
                return this;
            }
            for (var i = covers2.length; i--; ) {
                f.call(covers2[i]);
            }
            return this;
        };

        chart.click = function (f) {
            covers2.hide();
            covers.show();
            covers.click(f);
            return this;
        };

        chart.clickColumn = function (f) {
            covers.hide();
            covers2.show();
            covers2.click(f);
            return this;
        };

        chart.push(bars, covers, covers2);
        chart.bars = bars;
        chart.covers = covers;
        return chart;
    };

    Raphael.fn.ourbarchart = function (x, y, width, height, values, opts) {
        return new HBarchart(this, x, y, width, height, values, opts);
    };

})();
