var DEBUG = true;

function log(message)
{
    if(DEBUG)
        console.log(message)
}

$(document).ready(function(){

    $(".showmore").click(function () {
        if ($(this).text().indexOf("Остальные") !== -1) {
           $(".document-hidden").slideDown(200);
        $(this).html('<span>Скрыть</span>'); 
        }
        else {
          $(".document-hidden").slideUp(200);
          $(this).html('<span>Остальные 5</span>');  
        }
        return false;
    });

    /*страница system-backend/single-client-account*/
    $(".change-data").click(function () {
       var dis = $(this).parents().find('.disabled');
       $(dis).each(function () {
           $(this).prop('disabled', false);
       });
       $(this).css('display', 'none');
       $(this).parents().find('.save-data').css('display', 'block');
       return false;
    });

    $('.typeahead').typeahead({
          source: [ "Счет №38923", "Счет №28", "Иванов Карл Петрович", "Ницше Фридрих", "Акт №2310", "Оплата счета №9180", "Борисов Г.Н.", "Юридические формальности" ],
          items: 7,
          minLength: 1
    })

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        language: 'ru',
        weekStart: '1'
    });

    $('.datepicker')
       .on('changeDate', function(ev){
            $(this).prev().attr('value', $(this).data('date'));
    });

    $('.more-info').not('[class~="top-menu__more-info"]').popover({title:"Справка"})

        $(".linc-collapse_table_js").click(function(){
            var selector = $(this).attr("href")
            if ($(this).hasClass("linc-collapse__doun")){
                $(selector).show();
                $(this).removeClass("linc-collapse__doun");
                $(this).addClass("linc-collapse__up");
                return false;
            }
            if ($(this).hasClass("linc-collapse__up")){
                $(selector).hide();
                $(this).removeClass("linc-collapse__up");
                $(this).addClass("linc-collapse__doun");
                return false;
            }

        })

    $(".helper__quotes").click(function () {
        $(this).parents(".helper").toggleClass('open')
    });

    $(".popover.hide.fade").modal({
        "show":false,
        "backdrop":false
    });

    $('[rel="tooltip"]').tooltip();

    $(".popover.hide.fade").live('show', function () {
        $("body").addClass('popover-modal')
    }).on('hidden',function () {
            $(".modal-backdrop").remove();
            $("body").removeClass('popover-modal');
    });

    // сдвиг окон в конской таблице
    $(".order-list .popover.hide.fade").live('show', function (e) {
//        console.log($("[href=#"+e.target.id+"]").css("left"));
        var $targetEl= $("[href=#"+e.target.id+"]")
        $(this).css("left",$targetEl.css("left"))
        if (!$targetEl.data('offset')) {
        } else {
            var leftOfset = $targetEl.offset().left
            var leftOfsetThis = $(this).parent().offset().left
            var topOfset = $targetEl.offset().top
            var topOfsetThis = $(this).parent().offset().top
            var wheteTargetEl = $targetEl.width()
            $(this).css('left', leftOfset - leftOfsetThis + (wheteTargetEl/2))
            $(this).css('bottom', -topOfset + topOfsetThis -6)

        }
    });

    // пдсветка сегоднешего дня
    (function() {
       var list = $(".order-list");
       var indexTodty = list.find("th.today").index();
        if (indexTodty >= 0){
            list.find("tr").each(function () {
               $(this).find("td").eq(indexTodty).addClass("today");
            });
        }
    })();

    // подсветка ховера
    (function() {
        var list = $(".order-list");
        var elementList = $(".order-list th, .order-list td");
        elementList.hover(function () {
            var indexThis = $(this).index();

            list.find("tr").each(function () {
               $(this).find("td,th").eq(indexThis).addClass("hover");
            });
        }, function () {
            elementList.removeClass("hover");
        });
        $(".order-list__wraper-item").hover(function () {
            elementList.removeClass("hover");
            return false
        });
        $(".four th").hover(function () {
            elementList.removeClass("hover");
            return false
        });
            $('.order-list__grey th, .order-list__grey td').off('hover');
    })();

    //работа с деталями order-item-detal.html
    (function () {
        var colOrderListItem = $('.order-list__item')
        colOrderListItem.each(function () {
            var heightThis = $(this).outerHeight()
            $(this).parent('.order-list__wraper-item').height(heightThis)
        })
    })();

    //маркер для деталей order-item-detal.html
    (function () {
        if ( $('.order-list__item_alert').offset()){

        var posItemAlert = $('.order-list__item_alert').offset().top;
        var offsetPosItemAlert = posItemAlert + $('.order-list__item_alert').outerHeight();
        var ItemDetail = $(".order-list__item_detail")
        ItemDetail.each(function () {
            var $this = $(this);
            $this.next('.marker_line').height(
                $this.offset().top - offsetPosItemAlert
            )
        })
        }
    })();
    //Скрывалка  order-item-detal.html
    function cotForSmallParts() {
        var colItemDerail = $('.order-list__item_detail');
        $(".hoverSowe").removeClass("hoverSowe");
        colItemDerail.each(function () {
            var $this = $(this);
            $this.off('hover');
//            $(".hoverSowe").removeClass("hoverSowe");
            var whiteEl = $this.outerWidth();

            var whiteLink = $this.find(".link").outerWidth() + 50;
            var whiteBtn = $this.find(".btn ").outerWidth();
            var whiteDoc = $(document).width();

//            console.log($this.data('white'));
            if  (!$this.data('white')) {
                $this.data('white',$this.attr("style").match(/width:(\d+%)/)[1])
            }
//            var whiteElReal = $this.attr("style").match(/width:(\d+%)/)[1];
            $(window).resize(function(){
                        $this.css("width",$this.data('white'));
            });
            if (whiteLink > whiteEl || whiteBtn > whiteEl) {
                $this.addClass("hoverSowe");
                var setTimeoutFinish;
                $this.hover(function () {
                    var whiteThis;
                    if (whiteLink > whiteBtn ){
                        $this.css("width", whiteLink);
                        whiteThis = whiteLink;
                    } else {
                        $this.css("width",whiteBtn);
                        whiteThis=whiteBtn;
                    }
                    if ($this.offset().left + whiteThis > whiteDoc){
                        $this.css("marginLeft",whiteDoc - $this.offset().left - whiteThis - 20)
                    }
                    setTimeoutFinish = setTimeout($this.addClass("hoverSoweFinish"),200)
                }, function () {
                        $this.css("width",$this.data('white'));
                        $this.css("marginLeft",0);
                        $this.removeClass("hoverSoweFinish");
                        $this.parent().find('.popover').modal('hide');
//                    console.log( $this.parent().find('.popover '));
                });
            }
    })
    }
    cotForSmallParts();
    $(window).resize(function(){

        cotForSmallParts();
    });


    $('.mangerPanel_button').click(function(){
        var myDate = 3600*24*365;
        var curDate = new Date();
        //console.log(myDate);
        if ($(this).hasClass('active'))
        {
            $.cookie('managerTopPanel', true, {expire: 7});
            $(this).removeClass('active');
            $('.top-ponel__collapse-link-text').text('Отобразить панель');
        }
        else
        {
            $.cookie('managerTopPanel', false, {expires: 7});
            $(this).addClass('active');
            $('.top-ponel__collapse-link-text').text('Скрыть панель');
        }
    });


    // активы для кнопок кторе вызвают мобалное коно
    $(".helper__link").click(function () {
        $(this).addClass("active")
    });
    $("#informModal").on('hidden', function () {
        $(".helper__link").removeClass("active");
    });
    $(".top-ponel__notice > a").click(function () {
        $(this).addClass("active")
    });
    $("#noticeModal").on('hidden', function () {
        $(".top-ponel__notice > a").removeClass("active");
    });


    if ( $.hasOwnProperty('jPicker') ) {
        $('.Binded').jPicker();
    }

    $('.resize_auto').autoResize({
        // On resize:
        onResize : function() {
            $(this).css({opacity:0.8});
        },
        // After resize:
        animateCallback : function() {
            $(this).css({opacity:1});
        },
        // Quite slow animation:
        animateDuration : 300,
        // More extra space:
        extraSpace : 40
    });

//    $('#slider').slider();

    $(".sortable").each(function()
    {
        if($(this).get(0).tagName.toLowerCase() == "table")
        {
            if($(this).attr("id") == "menu-sort")
            {
                url = "/manager/menu";
            }
            if($(this).attr("id") == "catalog-sort")
            {
                url = "/manager/settings/catalogs/sort";
            }
            $(this).find("thead").find("tr").addClass("nodrag");

            $(this).tableDnD({
                dragHandle: "dragHandle",
                onDrop: function(table, row) {
                    postData = $.tableDnD.serialize();
                    $.post(url, postData, function(data){
                        //console.log(data)
                        if(data.error)
                            alert(data.error);
                    }, 'json');
                }
            });
        }
    });

    $(".sortable tr").hover(function() {
          $(this).find(".dragHandle").addClass('showDragHandle');
    }, function() {
          $(this).find(".dragHandle").removeClass('showDragHandle');
    });

    $('.top-menu__item_for-status .more-info').removeClass('more-info')
    $(".top-ponel__collapse-link").click(function(){
        if (!$(this).hasClass("active"))
        {
            if ( $.hasOwnProperty('cookie') ) {
                $.cookie('managerTopPanel', true);
            }
            $('.top-menu__item_for-status .top-menu__more-info').fadeOut(200);
            $(this).find("span").html("Отобразить панель");
        }
        else
        {
            if ( $.hasOwnProperty('cookie') ) {
                $.cookie('managerTopPanel', false);
            }
            $('.top-menu__item_for-status .top-menu__more-info').fadeIn(200);
            $(this).find("span").html("Скрыть панель");

        }

        $(this).toggleClass("active");

    });


    // вставка кнопки редоктирования
    $("[class *= 'tsb-edit']").prepend("<a href='#' class='edit'></a>");

    // открытие закрытие таблицы
    $(".tree-but").click(function(){
        $(this).parents(".item").toggleClass("active");
        return false;
    });

//    var datePickerBase = {
//        dateFormat: "dd.mm.yy",
//        monthNamesShort: ["Янв.", "Фев.", "Март", "Апр.", "Май", "Июнь", "Июль", "Авг.", "Сент.", "Окт.", "Ноя.", "Дек."],
//        monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
//        firstDay: 1,
//        dayNamesMin: ["ВС", "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ"],
//        dayNames: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
//        dayNamesShort: ["ВС", "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ"],
//        showOtherMonths: true,
//        selectOtherMonths: true
//    }
//
//
//    if ($.hasOwnProperty('datepicker') ) {
//        $(".data-selekt__input").each(function(){
//            var _this = this;
//            $(this).after($("<a>").addClass("data-selekt__cal").addClass("icon_data").click(function(){
//                $(_this).datepicker("show");
//            }));
//        }).datepicker(datePickerBase).mask("99.99.9999");
//
//        $( ".date-range-from, .date-range-to" ).datepicker( "destroy" ).datepicker($.extend(datePickerBase,{
//            onSelect: function( selectedDate ) {
//                var option = $(this).hasClass("date-range-from") ? "minDate" : "maxDate";
//                var instance = $( this ).data( "datepicker" );
//                var date = $.datepicker.parseDate(
//                    instance.settings.dateFormat ||
//                        $.datepicker._defaults.dateFormat,
//                    selectedDate, instance.settings );
//                $(this).parent().find(".date-range-from, .date-range-to").not( this ).datepicker( "option", option, date );
//            },
//            beforeShow: function(input, inst) {
//                var option = $(this).hasClass("date-range-to") ? "minDate" : "maxDate";
//                var instance = $(this).parent().find(".date-range-from, .date-range-to").not( this ).data( "datepicker");
//                var date = $.datepicker.parseDate(
//                    instance.settings.dateFormat ||
//                        $.datepicker._defaults.dateFormat,
//                    $(this).parent().find(".date-range-from, .date-range-to").not( this ).val(), instance.settings );
//                $(this).datepicker( "option", option, date );
//            }})
//
//        );
//    }

    /*$(".data-selekt li").click(function(){
        $(this).parent().find(".active").removeClass("active");
        $(this).addClass("active");
    })*/

    $("a.data-selek__text").click(function(){
        $.post($(this).attr("href"), {lastname:$("#lastname").val(), status:$('.statusfororder').val()}, updateData, "json");
        
        $(this).parents("ul").find(".active").removeClass("active");
        $(this).parent("li").addClass("active");

        return false;
    })
    $(".date-filter").submit(function(){
        $.post($(this).attr("action"), $(this).serialize(), updateData, "json");

        $(this).parents("ul").find(".active").removeClass("active");
        $(this).parent("li").prev().addClass("active");

        return false;
    });

    $('.modal').on('show', function(){
        $(this).css('top', $(window).scrollTop() + 100);
    });
});

/**
* При нажатии на сслыку "Показать еще N {items}"
*
* @returns {Boolean}
*/
function AExtendHandler()
{
    var rel = $(this).attr("rel");
    var parts = rel.split("-");
    var baseUrl = $(this).attr("href");
    if(baseUrl[baseUrl.length-1] != "/")
        baseUrl += "/";
    var url = baseUrl+parts[0]+"?date-from="+parts[1]+"&date-to="+parts[2]+"&start="+parts[3];

    that = this;
    $.get(url, null, ExtendHandler, "json");

    return false;
}

function ExtendHandler(data)
{
    updateData(data);
    if(data.getNext>0)
    {
        $(that).attr("rel", data.mode+"-"+data.dateFrom+"-"+data.dateTo+"-"+data.lastId);
        $(that).find("span").html(data.getNext);
        $(that).find("small").html(SelectByQuantity(data.getNext, "клиента", "клиентов", "клиентов"));

    }
    else
        $(that).parents(".add-more").parent().slideUp("slow",function(){$(this).remove()}); // Если все показаны уберем ссылку и снесем ее из DOM на всякий случай

}

function updateData(data)
{

    $("#"+data.container).find(".next_text span").html(data.getNext);

    if(data.isExtend)
        $("#"+data.table).append(data.response)
    else
        $("#"+data.container).html(data.response).find("a.load_extra").click(AExtendHandler);

    if(data.mode == "filter")
    {
        $("#"+data.container).prev().find(".date-range-from").val(data.dateFrom);
        $("#"+data.container).prev().find(".date-range-to").val(data.dateTo);
    }
    else
        $("#"+data.container).prev().find(".date-range-from, .date-range-to").val("");
}

function min(a, b)
{
    return (a<b) ? a : b;
}

/**
* Выбирает один из трех падежей для данного количеста
*/
function SelectByQuantity(quantity, case1, case2, case3)
{
    titles = [case1, case2, case3];
    cases = [2, 0, 1, 1, 1, 2];
    return titles[ (quantity%100 > 4 && quantity %100 < 20) ? 2 : cases[min(quantity%10, 5)] ];
}


//